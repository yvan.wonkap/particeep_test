let defaultState = {
    selectedMovies: { items: [] }, // Id + Nbr of likes + Nbr of dislikes
    selectedCategories: { items: [] },
    selectedPages: { page: 1 },
    selectedPageSize: { size: 4 }
};

let movieReducer = (state = defaultState, action) => {
    let newState = {...state };
    switch (action.type) {
        case 'DELETE_MOVIE': {
            let isHere = false;
            newState.selectedMovies.items.map(item => {
                if(item === action.payload.id){
                    isHere = true;
                }
            })
            if(!isHere){
                newState.selectedMovies = {
                    items: [...newState.selectedMovies.items, action.payload.id]
                } 
            }
            return newState;
        }
        default: 
            return state;
    }
};

export default movieReducer;