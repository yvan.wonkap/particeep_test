import React, { useEffect, useState } from 'react'
import Footer from '../../components/Footer/Footer'
import Row from '../../components/Row/Row'
import { Spin } from 'antd'
import { LoadingOutlined } from '@ant-design/icons';
import { movies$ } from '../../react-interview/movies'
import { dispatchAPI } from '../../contexts/apiContext';
import { useDispatch, useSelector } from 'react-redux'

const antIcon = <LoadingOutlined style={{ fontSize: 50 }} spin />;

const Home = () => {
  const [movies, setMovies] = useState([]);
  const [initialMovies, setInitialMovies] = useState([]);
  const [pageSize, setPageSize] = useState(4);
  const [pageNumber, setPageNumber] = useState(1);
  const [loading, setLoading] = useState(true);
  const basePath = 'https://image.tmdb.org/t/p/original'
  const deletedMovies = useSelector((state) => state.movieReducer.movieReducer.selectedMovies.items);
  
  // Load dynamically the movies
  useEffect(() => {
    setLoading(true);
    movies$.then((res) => {
        const output = [];
        res.map((movie) => {
            let isFound = false;
            deletedMovies.map(id => {
                if(id === movie.id){
                    isFound = true;
                }
            })
            if(!isFound){
                const outputStream = {};
                dispatchAPI(movie.title).then((details) => {
                    const { data } = details;
                    const image_movie = data ? data.results[0].poster_path : '';
                    const final_url = `${basePath}${image_movie}`;
                    outputStream.id = movie.id;
                    outputStream.category = movie.category;
                    outputStream.dislikes = movie.dislikes;
                    outputStream.likes = movie.likes;
                    outputStream.title = movie.title;
                    outputStream.img_url = final_url;
                    outputStream.isLiked = false;
                    output.push(outputStream);  
                })
            }
        })
        // Each request for getting a movie image can take approximately 200 ms before completion. 
        // So let's wait for movies$.length*200 ms for getting the full list of movies
        setTimeout(() => {
            setMovies(output);
            setLoading(false);
        }, res.length*200)
    })
  }, [deletedMovies]);

  return (
    <>
        {loading ?
                    <div className='loader_container'>
                         <Spin indicator={antIcon} size="large" tip="Loading..." />
                    </div>
                :
                    <>
                        <Row title="All Movies" movies={movies} setMovies={setMovies} pageSize={pageSize} pageNumber={pageNumber} />
                        <Footer nbr_movies={movies.length} pageSize={pageSize} setPageSize={setPageSize} setPageNumber={setPageNumber} />
                    </>
        }
    </>
  )
}

export default Home