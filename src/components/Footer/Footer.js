import React from 'react'
import './Footer.css'
import { Pagination } from 'antd'

const Footer = ({nbr_movies, pageSize, setPageSize, setPageNumber }) => {
    const onShowSizeChange = (current, pageSize) => {
        setPageSize(pageSize);
    }
    const onChange = (pageNumber) => {
        setPageNumber(pageNumber);
    }
    return (
        <>
            <div className='footer_container'>
                <Pagination 
                    showTotal={(total, range) => `${range[0]}-${range[1]} of ${total} items`} 
                    defaultCurrent={1} 
                    total={nbr_movies} 
                    showSizeChanger 
                    onChange={onChange} 
                    onShowSizeChange={onShowSizeChange} 
                    pageSize={pageSize}
                    pageSizeOptions={[4,8,12]}
                />
            </div>
            <p style={{ color: 'white', textAlign: 'center', marginTop: '10px' }}>Made with ❤ By Yvan Wonkap</p>
        </>
  )
}

export default Footer