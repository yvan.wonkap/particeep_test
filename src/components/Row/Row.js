import React, { useEffect, useState } from 'react'
import './Row.css'
import { ReactComponent as Delete } from '../../assets/icons/delete.svg'
import { ReactComponent as Like } from '../../assets/icons/like.svg'
import { ReactComponent as Dislike } from '../../assets/icons/dislike.svg'
import { Progress, Tag, Select } from 'antd'
import { useDispatch, useSelector } from 'react-redux'

const { Option } = Select;

const Row = ({ title, movies, setMovies, pageNumber, pageSize }) => {
  const [children, setChildren] = useState([]);
  const [categoryFilter, setCategoryFilter] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    const categories = [];
    const map = {};
    for (let i = 0; i < movies.length; i++) {
        if (map[movies[i].category]) {
            continue;
        } else {
            categories.push(<Option key={movies[i].category}>{movies[i].category}</Option>);
            map[movies[i].category] = true;
        }
    }
    setChildren(categories);
  }, [movies]);


  const deleteItem = (movie_id) => {
    dispatch({
        type: 'DELETE_MOVIE',
        payload: {
            id: movie_id
        }
    })
  }

  const changeLike = (movie_id) => {
    const oldMovies = movies;
    oldMovies.map((movie) => {
        if(movie.id === movie_id) {
            if(!movie.isLiked){
                movie.likes += 1;
                movie.isLiked = true;
            }
            else {
                movie.dislikes += 1; 
                movie.isLiked = false; 
            }
        }
    })
    console.log(oldMovies);
    setMovies([...oldMovies]);
  }

  const handleChange = (value) => {
    setCategoryFilter(value);
  }

  return (
    <div className="">
        <div className='header'>
            <h1 style={{ color: '#fff' }}>{title} ({movies.length})</h1>
            <Select
                mode="multiple"
                size='large'
                placeholder="Choose a category"
                onChange={handleChange}
                style={{ width: '250px', marginRight: '70px' }}
            >
                {children}
            </Select>
        </div>
        <div className="row__movies">
            {
                movies.filter((item_movie) => categoryFilter.length ? categoryFilter.includes(item_movie.category) : !categoryFilter.includes(item_movie.category)).slice((pageNumber - 1) * pageSize, pageNumber * pageSize).map((movie) => (
                    <div className='movie__card' key={movie.id}>
                        <Delete className="icon icon_delete" onClick={() => deleteItem(movie.id)} />
                        <img className="movie_img" src={movie.img_url} alt={movie.title} />
                        <div className='details'>
                            <h3 className="movie_title" style={{ color: '#fff' }}>{movie.title}</h3>
                            <Progress percent={Math.round((100*movie.likes/(movie.dislikes+movie.likes)), 10)} />
                            <div className='footer_card'>
                                <Tag color="#2db7f5">{movie.category}</Tag>
                                <div className='icon_rows'>
                                    {
                                        !movie.isLiked ?
                                            <Like className="icon__like" title="Like" onClick={() => changeLike(movie.id)} />
                                        :
                                            <Dislike className="icon__dislike" title="Dislike" onClick={() => changeLike(movie.id)} />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>  
                ))
            }
        </div>
    </div>
  )
}

export default Row