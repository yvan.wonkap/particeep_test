import './App.css';
import Home from './pages/Home/Home';
import 'antd/dist/antd.css';


// For this exercise, I use the TMDB API to fetch movie images
// Based on the movie title provided
const App = () => {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;
