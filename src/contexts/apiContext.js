import React from 'react'
import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: 'https://api.themoviedb.org/3',
    headers: { 'Content-Type': 'application/json' },
});

const movie_details_link = `/search/movie?api_key=b18db3186b5905ce213b4250f563b49f&query=`;
  
const dispatchAPI = async (movie_title) => {
    try {
    const result = await axiosInstance({
        url: `${movie_details_link}${movie_title}`,
        method: 'GET',
    });
    return result;
    } catch (e) {
    return e;
    }
};

export { dispatchAPI };